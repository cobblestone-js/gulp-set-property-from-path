'use strict';

var through = require('through2');
var dotted = require('dotted');

module.exports = function(params) {
    // Normalize the values in the parameters.
    params = params || {};
    params.path = params.path || /(\d{4}).(\d{2}).(\d{2})/;
    params.set = params.set || {};

    // Create and return the pipe.
    var pipe = through.obj(
        function(file, encoding, callback) {
            // Retrieve the value using dotted (to allow for dotted notation)
            // and compare the regexp against the path. If either don't work,
            // then we skip the file.
            var match = file.path.match(params.path);

            if (!match) {
                return callback(null, file);
            }

            // We have to loop through all the values in the setProperties
            // object and set each oen from the resulting regular expression.
            for (var key in params.set) {
                // We start with the substitution pattern.
                var value = params.set[key];

                // The $0 is the original value. This lets us do
                // 'Chapter $1: $0' to inject chapter titles into the values.
                var originalValue = dotted.getNested(file, key);

                if (value.indexOf('$0') >= 0 && originalValue === undefined) {
                    continue;
                }

                value = value.replace('$0', originalValue + '');

                // Get where we are going to be substituting the values and
                // then fake the $1 (etc) substitution inside the results.
                for (var index = 1; index < match.length; index++) {
                    value = value.replace('$' + index, match[index]);
                }

                // Set the value, even if it doesn't exist.
                dotted.setNested(file, key, value, { ensure:true });
            }

            // Callback so we can continue.
            callback(null, file);
        });

    return pipe;
};

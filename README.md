gulp-set-property-from-path
===========================

> A *gulp* plugin for setting one or more arbitrary values in a file based on its path.

## Setup and usage

Install `gulp-set-property-from-path` using `npm`:

```sh
npm i gulp-set-property-from-path
```

In your `gulpfile.js`:

```js
var gulp = require('gulp'),
    gulpGrayMatter = require('gulp-gray-matter'),
    gulpSetPropertyFromPath = require('gulp-set-property-from-path');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(gulpGrayMatter({ property: "data" }))
    .pipe(gulpSetPropertyFromPath({
        path: /(\d\d\d\d)-(\d\d)-(\d\d)/,
        set: {
            "data.year": "$1",
            "data.month": "$2",
            "data.day": "$3",
            "data.date": "$1-$2-$3"
        }
    }))
    .pipe(gulp.dest('./dest'));
});
```

A common workflow using this tool is to set dates based on a path (such as Jekyll-style blog postings) or inject a prefix in front of filenames.

## Options

### path

*RegExp*

Default: `/(\d{4}).(\d{2}).(\d{2})/`

The regular expression to scan the `file.path` attribute. If this does not match, then no changes are made to the object.

### set

*object*

Default: `{}`

An object that contains property/value pairs that should be set inside the object. There can be zero or more of these. The property name is the property of the file that needs to be set such as `data.date`.  It can be a nested property name like `data.inner.date`.

The value is a Perl-style replacement of a dollar (`$`) followed by a single digit number such as `$1`. This matches to the group of the path regular expression.

A special case is `$0` which puts the value of the previous property in its place. The below example prefixes all of the `title` properties with "Chapter X: " where X is the number in the filename. If there is a `$0` and the property does not exist, then that key in the `set` parameter is skipped but other values may be set.

```js
var gulp = require('gulp'),
    gulpGrayMatter = require('gulp-gray-matter'),
    gulpSetPropertyFromPath = require('gulp-set-property-from-path');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(gulpGrayMatter({ property: "data" }))
    .pipe(gulpSetPropertyFromPath({
        path: /chapter-(\d\d)/,
        set: {
            "data.title": "Chapter $1: $0"
        }
    }))
    .pipe(gulp.dest('./dest'));
});
```

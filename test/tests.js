'use strict';

var expect = require('expect');
var plugin = require('../lib/index');
var File = require('vinyl');

describe('gulp-set-property-from-path', function() {
    it('sets a single property from path', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: {},
            contents: Buffer.from('contents'),
            path: 'blog/1234/56-78-slug.markdown'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            path: /(\d{4}).(\d{2}).(\d{2})/,
            set: {
                'data.date': '$1-$2-$3'
            }
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.data.date).toEqual('1234-56-78');
            done();
        });
    });

    it('modify existing property', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: { date: 'a' },
            contents: Buffer.from('contents'),
            path: 'blog/1234/56-78-slug.markdown'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            path: /(\d{4}).(\d{2}).(\d{2})/,
            set: {
                'data.date': '$1-$0-$2-$3'
            }
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.data.date).toEqual('1234-a-56-78');
            done();
        });
    });

    it('skipping missing property', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: {},
            contents: Buffer.from('contents'),
            path: 'blog/1234/56-78-slug.markdown'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            path: /(\d{4}).(\d{2}).(\d{2})/,
            set: {
                'data.date': '$1-$0-$2-$3'
            }
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.data.date).toEqual(undefined);
            done();
        });
    });

    it('set nested property', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: {},
            contents: Buffer.from('contents'),
            path: 'blog/1234/56-78-slug.markdown'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            path: /(\d{4}).(\d{2}).(\d{2})/,
            set: {
                'data.inner.date': '$1-$2-$3'
            }
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.data.inner.date).toEqual('1234-56-78');
            done();
        });
    });
});
